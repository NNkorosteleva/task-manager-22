package ru.tsc.korosteleva.tm.command.user;

import ru.tsc.korosteleva.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "user-logout";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Log out";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOG OUT]");
        getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
