package ru.tsc.korosteleva.tm.command.project;

import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListAllUsersCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list-all-users";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Show project list all users.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST ALL USERS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        renderProject(projects);
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
