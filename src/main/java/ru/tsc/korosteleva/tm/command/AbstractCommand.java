package ru.tsc.korosteleva.tm.command;

import ru.tsc.korosteleva.tm.api.model.ICommand;
import ru.tsc.korosteleva.tm.api.service.IAuthService;
import ru.tsc.korosteleva.tm.api.service.IServiceLocator;
import ru.tsc.korosteleva.tm.api.service.IUserService;
import ru.tsc.korosteleva.tm.enumerated.Role;
import ru.tsc.korosteleva.tm.model.User;

public abstract class AbstractCommand implements ICommand {

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    public abstract Role[] getRoles();

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    public String getUserId() {
        return getAuthService().getUserId();
    }

    public String getLogin(final String userId) {
        final User user = getUserService().findOneById(userId);
        return user.getLogin();
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;
    }
}
