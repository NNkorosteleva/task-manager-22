package ru.tsc.korosteleva.tm.component;

import ru.tsc.korosteleva.tm.api.repository.*;
import ru.tsc.korosteleva.tm.api.service.*;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.command.project.*;
import ru.tsc.korosteleva.tm.command.system.*;
import ru.tsc.korosteleva.tm.command.task.*;
import ru.tsc.korosteleva.tm.command.user.*;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.korosteleva.tm.exception.system.CommandNotSupportedException;
import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.model.Task;
import ru.tsc.korosteleva.tm.repository.*;
import ru.tsc.korosteleva.tm.service.*;
import ru.tsc.korosteleva.tm.util.DateUtil;
import ru.tsc.korosteleva.tm.util.TerminalUtil;
import ru.tsc.korosteleva.tm.model.User;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskRepository projectTaskRepository = new ProjectTaskRepository(projectRepository, taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectTaskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository(projectRepository, taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(userService, authRepository);

    {
        registry(new AboutCommand());
        registry(new VersionCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListAllUsersCommand());
        registry(new TaskListCommand());
        registry(new TaskClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListAllUsersCommand());
        registry(new ProjectListCommand());
        registry(new ProjectClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new TaskListByProjectCommand());
        registry(new TaskBindToProject());
        registry(new TaskUnbindToProject());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserViewProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserFindAllCommand());
        registry(new UserFindByEmailCommand());
        registry(new UserFindByIdCommand());
        registry(new UserFindByLoginCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new ExitCommand());
    }

    public void run(final String[] args) {
        if (processArgument(args)) close();
        initData();
        showProcess();
    }

    public void showProcess() {
        showWelcome();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        try {
            processArgument(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("[FAIL]");
            close();
        }
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    public void showWelcome() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void close() {
        System.exit(0);
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        User usualUser = userService.create("test", "test", "test@test.te");
        User adminUser = userService.create("admin", "admin", "admin@admin.ad", "ADMIN");
        taskService.create(usualUser.getId(), "DEMO TASK", "DEMO DESCRIPTION", DateUtil.toDate("10.01.2019"), DateUtil.toDate("10.10.2019")).setStatus(Status.NOT_STARTED);
        taskService.create(adminUser.getId(), "TEST TASK", "TEST DESCRIPTION", DateUtil.toDate("09.12.2020"), DateUtil.toDate("10.12.2020")).setStatus(Status.IN_PROGRESS);
        taskService.create(adminUser.getId(), "CHECK TASK", "CHECK DESCRIPTION", DateUtil.toDate("10.01.2019"), DateUtil.toDate("20.01.2019")).setStatus(Status.COMPLETED);
        taskService.create(usualUser.getId(), "CHECKED TASK", "CHECKED DESCRIPTION", DateUtil.toDate("01.04.2019"), DateUtil.toDate("05.04.2019")).setStatus(Status.IN_PROGRESS);
        projectService.create(usualUser.getId(), "DEMO PROJECT", "DEMO DESCRIPTION", DateUtil.toDate("12.12.2019"), DateUtil.toDate("12.12.2020")).setStatus(Status.NOT_STARTED);
        projectService.create(usualUser.getId(), "TEST PROJECT", "TEST DESCRIPTION", DateUtil.toDate("11.11.2020"), DateUtil.toDate("15.11.2020")).setStatus(Status.COMPLETED);
        projectService.create(usualUser.getId(), "CHECK PROJECT", "CHECK DESCRIPTION", DateUtil.toDate("10.08.2019"), DateUtil.toDate("10.10.2019")).setStatus(Status.IN_PROGRESS);
        projectService.create(adminUser.getId(), "CHECKED PROJECT", "CHECKED DESCRIPTION", DateUtil.toDate("05.07.2021"), DateUtil.toDate("10.07.2021")).setStatus(Status.COMPLETED);
    }

}