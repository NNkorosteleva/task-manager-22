package ru.tsc.korosteleva.tm.repository;

import ru.tsc.korosteleva.tm.api.repository.IProjectRepository;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Project;

import java.util.Date;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String userId, final String name) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @Override
    public Project create(final String userId,
                          final String name,
                          final String description) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String userId,
                          final String name,
                          final String description,
                          final Date dateBegin,
                          final Date dateEnd) {
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return add(project);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        return records.stream()
                .filter(project -> userId.equals(project.getUserId()) && name.equals(project.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Project updateById(final String userId,
                              final String id,
                              final String name,
                              final String description) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId,
                                 final Integer index,
                                 final String name,
                                 final String description) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        records.remove(project);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId,
                                           final String id,
                                           final Status status) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId,
                                              final Integer index,
                                              final Status status) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}