package ru.tsc.korosteleva.tm.api.model;

public interface ICommand {

    String getName();

    String getArgument();

    String getDescription();

}
