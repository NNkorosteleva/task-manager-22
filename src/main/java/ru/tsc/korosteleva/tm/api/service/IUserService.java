package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password, String email);

    User create(String login, String password, String email, String role);

    User updateUser(String id, String firstName, String lastName, String middleName);

    User setPassword(String id, String password);

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
