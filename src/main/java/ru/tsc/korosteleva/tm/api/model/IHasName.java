package ru.tsc.korosteleva.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
