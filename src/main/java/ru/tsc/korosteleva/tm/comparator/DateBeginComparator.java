package ru.tsc.korosteleva.tm.comparator;

import ru.tsc.korosteleva.tm.api.model.IHasDateBegin;

import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(final IHasDateBegin dateBegin1, final IHasDateBegin dateBegin2) {
        if (dateBegin1 == null || dateBegin2 == null) return 0;
        if (dateBegin1.getDateBegin() == null || dateBegin2.getDateBegin() == null) return 0;
        return dateBegin1.getDateBegin().compareTo(dateBegin2.getDateBegin());
    }

}
