package ru.tsc.korosteleva.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

}
