package ru.tsc.korosteleva.tm.exception.user;

public final class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("Error! User id is empty.");
    }

}
