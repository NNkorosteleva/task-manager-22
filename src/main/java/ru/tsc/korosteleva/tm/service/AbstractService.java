package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.IRepository;
import ru.tsc.korosteleva.tm.api.service.IService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.exception.entity.ModelNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.field.IndexIncorrectException;
import ru.tsc.korosteleva.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.add(model);

    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort);
    }

    @Override
    public M findOneById(final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneById(id));
        return model.orElseThrow(ModelNotFoundException::new);

    }

    @Override
    public M findOneByIndex(final Integer index) {
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize())
                .orElseThrow(IndexIncorrectException::new);
        Optional<M> model = Optional.ofNullable(repository.findOneByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M remove(final M model) {
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.remove(model);
    }

    @Override
    public M removeById(final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional<M> model = Optional.ofNullable(repository.removeById(id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public M removeByIndex(final Integer index) {
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize())
                .orElseThrow(IndexIncorrectException::new);
        Optional<M> model = Optional.ofNullable(repository.removeByIndex(index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public void removeAll(Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        repository.removeAll(collection);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public long getSize() {
        return repository.getSize();
    }

    @Override
    public boolean existsById(final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return repository.existsById(id);
    }

}
