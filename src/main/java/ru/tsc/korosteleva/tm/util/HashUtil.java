package ru.tsc.korosteleva.tm.util;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    String SECRET = "1234567895";

    Integer ITERATION = 12354;

    static String salt(final String val) {
        if (val == null) return null;
        String result = val;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(String val) {
        if (val == null) return null;
        try {
            java.security.MessageDigest md =
                    java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(val.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

}
